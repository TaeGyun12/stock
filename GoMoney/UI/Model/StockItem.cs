﻿using System.Collections.Generic;

namespace UI.Model
{
    /// <summary>
    /// 주식 종목 코드 별로 관리하는 데이터 관리 클래스
    /// </summary>
    public class StockItemManager
    {
        public Dictionary<string, StockItem> stockItemDic;
        public StockItemManager()
        {
            stockItemDic = new Dictionary<string, StockItem>();
        }
    }

    /// <summary>
    /// 주식 종목에 대한 정보를 갖고 있는 클래스
    /// </summary>
    public class StockItem
    {
        public string 종목명;
        public double 등락율;
        public int 대비;
        public double 현재가;
        public double 거래량;

        public StockItem(string 종목명, double 등락율, int 대비, double 현재가, double 거래량)
        {
            this.종목명 = 종목명;
            this.등락율 = 등락율;
            this.대비 = 대비;
            this.현재가 = 현재가;
            this.거래량 = 거래량;
        }
    }
}
