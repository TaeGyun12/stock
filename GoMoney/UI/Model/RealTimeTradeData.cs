﻿namespace UI.Model
{
    /// <summary>
    /// 실시간 체결 Data를 나타내는 Class
    /// </summary>
    public class RealTimeTradeData
    {
        public string 종목코드;
        public double 현재가;
        public int 거래시간;
        public double 거래량;

        public RealTimeTradeData(string 종목코드, double 현재가, int 거래시간, double 거래량)
        {
            this.종목코드 = 종목코드;
            this.현재가 = 현재가;
            this.거래량 = 거래량;
            this.거래시간 = 거래시간;
        }
    }
}
