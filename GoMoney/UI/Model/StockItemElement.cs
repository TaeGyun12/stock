﻿using System;
using System.Collections.Generic;
using UI.Model.EventModel;

namespace UI.Model
{
    public class StockItemElement
    {
        #region STATIC
        private static StockItemElement stockItemElement;
        private static object lockObject = new object();
        #endregion
        private StockItemElement() 
        {
            주식사전 = new Dictionary<string, Dictionary<double, 체결정보>>();
        }
  
        #region EVENT
        public event EventHandler<MonitorStockOddEvnetArgs> OnReceiveStockOddHandler;
        #endregion

        //key : 종목 코드, value : 체결사전(key : 체결량, value: 체결정보)
        public Dictionary<string, Dictionary<double, 체결정보>> 주식사전;
        public Dictionary<double, 체결정보> 체결사전;
        public static StockItemElement Instance()
        {
            if(stockItemElement == null)
            {
                lock(lockObject)
                {
                    if(stockItemElement == null)
                    {
                        stockItemElement = new StockItemElement();
                        return stockItemElement;
                    }
                }
            }
            return stockItemElement;
        }
        public string SelectedStockCode { get; set; }

        /// <summary>
        /// 특정 종목의 실시간 체결 정보를 가져와서, 이 정보를 추가하는 Call Back
        /// </summary>
        /// <param name="data"></param>
        public void ThreadPoolCallBack(object data)
        {
            RealTimeTradeData realTimeTradeData = data as RealTimeTradeData;
            if(realTimeTradeData != null)
            {
                AddRealTradeDataToStockOddDic(realTimeTradeData.종목코드, realTimeTradeData.현재가, realTimeTradeData.거래량, realTimeTradeData.거래시간);
            }
        }

        /// <summary>
        /// 실시간 체결 정보를 받은 종목에 대해서 단주를 찾기위해 "단주 찾기 사전"에 추가하는 함수.
        /// </summary>
        /// <param name="종목코드"></param>
        /// <param name="현재가"></param>
        /// <param name="체결량"></param>
        /// <param name="체결시간"></param>
        private void AddRealTradeDataToStockOddDic(string 종목코드, double 현재가, double 체결량, int 체결시간)
        {
            if(주식사전.ContainsKey(종목코드))
            {
                if(주식사전[종목코드].ContainsKey(체결량))
                {
                    주식사전[종목코드][체결량].AddSameOddTransaction(현재가 * 체결량, 체결시간);
                }
                else
                {
                    주식사전[종목코드].Add(체결량, new 체결정보(종목코드, 현재가 * 체결량, 체결량, 체결시간));
                }
            }
            else
            {
                체결사전 = new Dictionary<double, 체결정보>();
                체결사전.Add(체결량, new 체결정보(종목코드, 현재가 * 체결량, 체결량, 체결시간));
                주식사전.Add(종목코드, 체결사전);
            }
        }

        public class 체결정보
        {
            private static readonly int JUDGE_STOCK_ODD_COUNT = 3; //최소 단주 등장 횟수
            private static readonly int MINIMUM_TIME_SPAN = 30; //최소 시차 

            public string 종목코드;
            public double 체결량;
            public int 체결시간;
            public double 체결금액;
            public List<int> 체결시간리스트;
            public int latestTimeIndex;   //특정 정보
            public Dictionary<int, StockOddLotInfo> 단주주기사전; //키 : 주기, 값: 단주주기정보객체
            public Dictionary<int, int> 단주주기임시사전; //키 : 주기, 값 : 반복횟수
            public StockItemElement stockItemElement = StockItemElement.Instance();

            public 체결정보(string 종목코드, double 체결금액, double 체결량, int 체결시간)
            {
                this.종목코드 = 종목코드;
                this.체결금액 = 체결금액;
                this.체결량 = 체결량;
                this.체결시간 = 체결시간;
                //체결 시간 리스트 초기화
                체결시간리스트 = new List<int>();
                latestTimeIndex = 0;
                체결시간리스트.Add(체결시간);
                //단주주기 사전 초기화
                단주주기사전 = new Dictionary<int, StockOddLotInfo>();
                단주주기임시사전 = new Dictionary<int, int>();
                단주주기임시사전.Add(0, 1);
            }

            /// <summary>
            /// 동일한 거래량(주)에 대한 새로운 체결 정보를 추가합니다.
            /// </summary>
            /// <param name="체결금액"></param>
            /// <param name="체결시간"></param>
            public void AddSameOddTransaction(double 체결금액, int 체결시간)
            {
                if (!체결시간리스트.Contains(체결시간))
                {
                    latestTimeIndex++;
                    this.체결금액 = 체결금액;
                    체결시간리스트.Add(체결시간);
                    calculatePeriod();
                }
            }

            /// <summary>
            /// 단주를 찾는 알고리즘
            /// </summary>
            private void calculatePeriod()
            {
                int properTransactionTime = (체결시간리스트[latestTimeIndex] + 체결시간리스트[0]) / 2;
                for (int timeIndex = latestTimeIndex - 1; timeIndex >= 0 && 체결시간리스트[timeIndex] >= properTransactionTime; timeIndex--)
                {
                    //가장 최신의 매수 시간과 그 이전 매수 시간의 차이
                    int 시차 = 체결시간리스트[latestTimeIndex] - 체결시간리스트[timeIndex];

                    //임의로 최소 시차 설정이 가능
                    if (시차 >= MINIMUM_TIME_SPAN)
                    {
                        int expectedPreviousBuyingTime = 체결시간리스트[timeIndex] - 시차;

                        if (체결시간리스트.Contains(expectedPreviousBuyingTime)) //이전에도 이 주기로 체결이 이루어졌다!
                        {
                            IncrementTransactionData(시차, 시차, 체결시간리스트[latestTimeIndex]);
                            break;
                        }
                        else if (체결시간리스트.Contains(expectedPreviousBuyingTime - 1))
                        {
                            //시차가 '-1'이 나서 +1
                            IncrementTransactionData(시차, 시차 + 1, 체결시간리스트[latestTimeIndex]);
                            break;
                        }
                        else if (체결시간리스트.Contains(expectedPreviousBuyingTime + 1))
                        {
                            IncrementTransactionData(시차, 시차 - 1, 체결시간리스트[latestTimeIndex]);
                            break;
                        }
                    }
                }
            }

            /// <summary>
            /// 미묘한 시차로 인해서 +1, -1에서의 Term이 발생함 (개수도 미묘하게 차이가 날 수 있음)
            /// 그걸 보정하기 위한 과정
            /// </summary>
            /// <param name="시차"></param>
            /// <param name="비슷한시차"></param>
            /// <param name="체결시간"></param>
            private void IncrementTransactionData(int 시차, int 비슷한시차, int 최근체결시간)
            {
                int 주기;

                if (시차 != 비슷한시차)
                {
                    if (단주주기임시사전.ContainsKey(시차) && !단주주기임시사전.ContainsKey(비슷한시차))
                    {
                        주기 = 시차;
                        단주주기임시사전[주기] += 1;
                    }
                    else if (!단주주기임시사전.ContainsKey(시차) && 단주주기임시사전.ContainsKey(비슷한시차))
                    {
                        주기 = 비슷한시차;
                        단주주기임시사전[주기] += 1;
                    }
                    else if (단주주기임시사전.ContainsKey(시차) && 단주주기임시사전.ContainsKey(비슷한시차))
                    {
                        주기 = 시차;
                        단주주기임시사전[주기] += 1;
                    }
                    else
                    {
                        주기 = 시차;
                        단주주기임시사전.Add(주기, 2); //지금 발견된 값과 이전값이 2개라서
                    }
                }
                else
                {
                    if (!단주주기임시사전.ContainsKey(시차)) //현재 시차가 없다!
                    {
                        if (단주주기임시사전.ContainsKey(시차 - 1)) //그런데 -1 인 시차는 있었으면 이걸로 확정!
                        {
                            주기 = 시차 - 1;
                            단주주기임시사전[주기] += 1;
                        }
                        else
                        {
                            주기 = 시차;
                            단주주기임시사전.Add(주기, 2); //지금 발견된 값과 이전값이 2개라서
                        }
                    }
                    else
                    {
                        주기 = 시차;
                        단주주기임시사전[주기] += 1;
                    }
                }

                //3번이상 나오면 이건 단주다
                if (단주주기임시사전[주기] > JUDGE_STOCK_ODD_COUNT)
                {
                    //처음 등록일 경우
                    if (!단주주기사전.ContainsKey(주기))
                    {
                        int 처음체결시간 = 최근체결시간 - (주기 * JUDGE_STOCK_ODD_COUNT);
                        double 누적거래대금 = 체결금액 * (JUDGE_STOCK_ODD_COUNT + 1);
                        StockOddLotInfo stockOddLotInfo = new StockOddLotInfo(단주주기임시사전[주기], 최근체결시간, 처음체결시간, 누적거래대금);
                        단주주기사전.Add(주기, stockOddLotInfo);
                    }
                    else //추가될 경우
                    {
                        단주주기사전[주기].반복횟수 = 단주주기임시사전[주기];
                        단주주기사전[주기].마지막체결시간 = 최근체결시간;
                        단주주기사전[주기].누적거래대금 += 체결금액;
                    }
                    if(stockItemElement.SelectedStockCode != null && stockItemElement.SelectedStockCode.Equals(종목코드))
                    {
                        stockItemElement.OnReceiveStockOddHandler?.Invoke(this, new MonitorStockOddEvnetArgs(종목코드));
                    }
                }
            }
        }
    }
}
