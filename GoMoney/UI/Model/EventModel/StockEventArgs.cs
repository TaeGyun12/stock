﻿using System;
namespace UI.Model.EventModel
{
    public class StockEventArgs : EventArgs
    {
        public StockItemManager stockItemManager;
        public StockEventArgs(StockItemManager stockItemManager)
        {
            this.stockItemManager = stockItemManager;
        }
    }
}
