﻿namespace UI.Model
{
    /// <summary>
    /// 단주 정보를 저장하는 Class
    /// </summary>
    public class StockOddLotInfo
    {
        public int 반복횟수;
        public int 마지막체결시간;
        public int 처음체결시간;
        public double 누적거래대금;

        public StockOddLotInfo(int 반복횟수, int 마지막체결시간, int 처음체결시간, double 누적거래대금)
        {
            this.반복횟수 = 반복횟수;
            this.마지막체결시간 = 마지막체결시간;
            this.처음체결시간 = 처음체결시간;
            this.누적거래대금 = 누적거래대금;
        }
    }
}
