﻿using System.Collections.Generic;
using System.Windows.Forms;
using UI.Connector;
using UI.Model;
using UI.Model.EventModel;

namespace UI
{
    public partial class Main : Form
    {
        string loginType;
        AxKHOpenAPILib.AxKHOpenAPI axKHOpenAPI1;
        int screenNum;
        UI ui;
        KHOpenAPIConnector apiConnector;
        StockItemElement stockItemElement; //단주관련 클래스
        StockItemManager stockItemManager; //관심등록 관련 클래스

        public Main(string _loginType, UI _ui)
        {
            InitializeComponent();
            stockItemElement = StockItemElement.Instance();
            loginType = _loginType;
            ui = _ui;
            screenNum = 5000;
            apiConnector = new KHOpenAPIConnector();
            apiConnector.SetOption(axKHOpenAPI1);
            apiConnector.OnReceiveRealDataHandler += OnReceiveRealStockData;
            stockItemElement.OnReceiveStockOddHandler += OnReceiveSelectedStockItemEvent;
        }

        /// <summary>
        /// 스크린 번호를 얻어온다
        /// </summary>
        /// <returns></returns>
        private  string GetScreenNumber()
        {
            if(screenNum < 9999)
            {
                screenNum++;
            }
            else
            {
                screenNum = 5000;
            }
            return screenNum.ToString();
        }

        /// <summary>
        /// Main 화면 Loading EventHandler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_Load(object sender, System.EventArgs e)
        {
            if(!(axKHOpenAPI1.CommConnect() == 0))
            {
                MessageBox.Show("Login Failed");
            }
        }
        
        /// <summary>
        /// Main화면 종료 EventHandler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            ui.Close();
        }

        /// <summary>
        /// Login 후, Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axKHOpenAPI1_OnEventConnect(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnEventConnectEvent e)
        {
            string accountNumber = axKHOpenAPI1.GetLoginInfo(LoginOption.ACCNO);

            Text = $"[{loginType}투자, 계좌번호 : {accountNumber.Split(new char[] { ';' })[0]}]";
        }

        /// <summary>
        /// 종목 코드를 쓰고 관심등록 버튼 Click EventHandler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 종목코드등록버튼_Click(object sender, System.EventArgs e)
        {
            string code = 종목코드.Text;
            if(string.IsNullOrEmpty(code))
            {
                MessageBox.Show("종목코드가 Null Or Empty입니다!!");
                return;
            }
            long reuslt = axKHOpenAPI1.SetRealReg(GetScreenNumber(), code, "9001;10", "0");
            if(reuslt != 0)
            {
                MessageBox.Show("종목코드를 관심등록 하는데 실패했습니다");
            }
        }

        /// <summary>
        /// 관심등록 종목들을 대상으로 마지막 체결정보 업데이트해주는 이벤트 핸들러 (실시간)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnReceiveRealStockData(object sender, StockEventArgs e)
        {
            stockItemManager = e.stockItemManager;
            int idx = 0;
            foreach(KeyValuePair<string, StockItem> stockItem in stockItemManager.stockItemDic)
            {
                if(SavedStockGridVeiw.RowCount < stockItemManager.stockItemDic.Count)
                {
                    SavedStockGridVeiw.Rows.Add();
                }
                SavedStockGridVeiw.Rows[idx].Cells[0].Value = stockItem.Key;
                SavedStockGridVeiw.Rows[idx].Cells[1].Value = stockItem.Value.종목명;
                SavedStockGridVeiw.Rows[idx].Cells[2].Value = stockItem.Value.현재가;
                SavedStockGridVeiw.Rows[idx].Cells[3].Value = stockItem.Value.등락율;
                SavedStockGridVeiw.Rows[idx].Cells[4].Value = stockItem.Value.대비;
                SavedStockGridVeiw.Rows[idx].Cells[5].Value = stockItem.Value.거래량;
                idx++;
            }
        }

        private void SavedStockGridVeiw_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex >= 0)
            {
                if(e.ColumnIndex == 7)
                {
                    string 종목코드 = SavedStockGridVeiw.Rows[e.RowIndex].Cells[0].Value.ToString();
                    string 종목명 = SavedStockGridVeiw.Rows[e.RowIndex].Cells[1].Value.ToString();
                    if (e.Button == MouseButtons.Left)
                    {
                        //단주 종목 그리드뷰 보이기
                        if(!종목명label.Text.Equals(종목명))
                        {
                            종목명label.Text = 종목명;
                            stockItemElement.SelectedStockCode = 종목코드;   
                            SetStockOddLotGridView(종목코드);
                        }
                    }
                    else if(e.Button == MouseButtons.Right)
                    {
                        axKHOpenAPI1.SetRealRemove("ALL", 종목코드);
                        if(stockItemManager.stockItemDic.ContainsKey(종목코드))
                        {
                            stockItemManager.stockItemDic.Remove(종목코드);
                        }
                    }
                }
            }
        }

        public void SetStockOddLotGridView(string 종목코드)
        {
            int index = 0;
            단주GridView.Rows.Clear();
            if(stockItemElement.주식사전.ContainsKey(종목코드))
            {
                foreach(KeyValuePair<double, StockItemElement.체결정보> 체결사전 in stockItemElement.주식사전[종목코드])
                {
                    foreach(KeyValuePair<int, StockOddLotInfo> 단주사전 in 체결사전.Value.단주주기사전)
                    {
                        if(단주GridView.RowCount < stockItemElement.주식사전[종목코드].Count * 체결사전.Value.단주주기사전.Count)
                        {
                            단주GridView.Rows.Add();
                        }
                        int 마지막hour = 단주사전.Value.마지막체결시간 / 3600;
                        int 마지막min = (단주사전.Value.마지막체결시간 - 마지막hour * 3600) / 60;
                        int 마지막sec = 단주사전.Value.마지막체결시간 - 3600 * 마지막hour - 60 * 마지막min;

                        int 처음hour = 단주사전.Value.처음체결시간 / 3600;
                        int 처음min = (단주사전.Value.처음체결시간 - 마지막hour * 3600) / 60;
                        int 처음sec = 단주사전.Value.처음체결시간 - 3600 * 처음hour - 60 * 처음min;

                        int 주기시간 = 단주사전.Key;
                        int 주기분 = 0;
                        int 주기초 = 0;
                        if(주기시간 > 60)
                        {
                            주기분 = 주기시간 / 60;
                            주기초 = 주기시간 % 60;
                        }
                        else
                        {
                            주기초 = 주기시간;
                        }
                        단주GridView.Rows[index].Cells[0].Value = 체결사전.Key.ToString();
                        단주GridView.Rows[index].Cells[1].Value = 단주사전.Value.반복횟수.ToString();
                        단주GridView.Rows[index].Cells[2].Value = 단주사전.Value.누적거래대금.ToString();
                        if(주기분 > 0)
                        {
                            단주GridView.Rows[index].Cells[3].Value = 주기분 + ":" + 주기초;
                        }
                        else
                        {
                            단주GridView.Rows[index].Cells[3].Value = 주기초;
                        }
                        단주GridView.Rows[index].Cells[4].Value = 처음hour + ":" + 처음min + ":" + 처음sec;
                        단주GridView.Rows[index].Cells[5].Value = 마지막hour + ":" + 마지막min + ":" + 마지막sec;
                        
                        index++;
                    }
                }
            }
        }
        public void OnReceiveSelectedStockItemEvent(object sender, MonitorStockOddEvnetArgs e)
        {
            //수정 필요
            string 종목코드 = e.종목코드;
            int index = 0;
            int dataCnt = 0;
            foreach (KeyValuePair<double, StockItemElement.체결정보> 체결사전 in stockItemElement.주식사전[종목코드])
            {
                dataCnt += 체결사전.Value.단주주기사전.Count;
                foreach (KeyValuePair<int, StockOddLotInfo> 단주사전 in 체결사전.Value.단주주기사전)
                {
                    if (단주GridView.RowCount < dataCnt)
                    {
                        단주GridView.Rows.Add();
                    }
                    int 마지막hour = 단주사전.Value.마지막체결시간 / 3600;
                    int 마지막min = (단주사전.Value.마지막체결시간 - 마지막hour * 3600) / 60;
                    int 마지막sec = 단주사전.Value.마지막체결시간 - 3600 * 마지막hour - 60 * 마지막min;

                    int 처음hour = 단주사전.Value.처음체결시간 / 3600;
                    int 처음min = (단주사전.Value.처음체결시간 - 마지막hour * 3600) / 60;
                    int 처음sec = 단주사전.Value.처음체결시간 - 3600 * 처음hour - 60 * 처음min;

                    int 주기시간 = 단주사전.Key;
                    int 주기분 = 0;
                    int 주기초 = 0;
                    if (주기시간 > 60)
                    {
                        주기분 = 주기시간 / 60;
                        주기초 = 주기시간 % 60;
                    }
                    else
                    {
                        주기초 = 주기시간;
                    }
                    단주GridView.Rows[index].Cells[0].Value = 체결사전.Key.ToString();
                    단주GridView.Rows[index].Cells[1].Value = 단주사전.Value.반복횟수.ToString();
                    단주GridView.Rows[index].Cells[2].Value = 단주사전.Value.누적거래대금.ToString();
                    if (주기분 > 0)
                    {
                        단주GridView.Rows[index].Cells[3].Value = 주기분 + ":" + 주기초;
                    }
                    else
                    {
                        단주GridView.Rows[index].Cells[3].Value = 주기초;
                    }
                    단주GridView.Rows[index].Cells[4].Value = 처음hour + ":" + 처음min + ":" + 처음sec;
                    단주GridView.Rows[index].Cells[5].Value = 마지막hour + ":" + 마지막min + ":" + 마지막sec;

                    index++;
                }
            }
        }
    }
}
