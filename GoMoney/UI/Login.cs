﻿using System;
using System.Windows.Forms;

namespace UI
{
    public partial class UI : Form
    {
        string loginType;
        public UI()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            loginType = comboBox1.Text;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(loginType))
            {
                MessageBox.Show("Select Type!!");
                return;
            }
            Hide();
            Main main = new Main(loginType, this);

            main.ShowDialog();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
