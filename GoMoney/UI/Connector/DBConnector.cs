﻿using MySql.Data.MySqlClient;
using System;

namespace UI.Connector
{
	public class DBConnector
	{
		private static object obj = new object();
		private static DBConnector dBConnector;
		private MySqlConnection connection;

		private DBConnector() 
		{
			string connectionString = $"SERVER=localhost;DATABASE=stock;UID=root;PASSWORD=qwer!6819400;";
			connection = new MySqlConnection(connectionString);
			openConnection();
		}
		public static DBConnector INSTANCE
		{
			get
			{
				if(dBConnector == null)
				{
					lock(obj)
					{
						if(dBConnector == null)
						{
							dBConnector = new DBConnector();
							return dBConnector;
						}
					}
				}
				return dBConnector;
			}
		}

		private void openConnection()
		{
			try
			{
				connection.Open();
			}
			catch(Exception e)
			{
				
			}
		}
	}
}
