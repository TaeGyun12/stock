﻿using System;
using System.Threading;
using UI.Model;
using UI.Model.EventModel;

namespace UI.Connector
{
    class KHOpenAPIConnector
    {
        private AxKHOpenAPILib.AxKHOpenAPI axKHOpenAPI;
        private StockItemManager stockItemManager;
        public event EventHandler<StockEventArgs> OnReceiveRealDataHandler;
        StockItemElement stockItemElement;
        public KHOpenAPIConnector()
        {
            stockItemManager = new StockItemManager();
            stockItemElement = StockItemElement.Instance();
        }

        public void SetOption(AxKHOpenAPILib.AxKHOpenAPI library)
        {
            axKHOpenAPI = library;
            axKHOpenAPI.OnReceiveRealData += AxKHOpenAPI_OnReceiveRealData;
        }

        /// <summary>
        /// 관심등록한 종목들의 실시간 Data를 전달 받는 EventHandler;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AxKHOpenAPI_OnReceiveRealData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            //주식 체결 관련 Data
            if(e.sRealType == "주식체결")
            {
                double 거래량 = Double.Parse(axKHOpenAPI.GetCommRealData(e.sRealType, 15));
                int 체결시간 = Int32.Parse(axKHOpenAPI.GetCommRealData(e.sRealType, 20));
                double 현재가 = Double.Parse(axKHOpenAPI.GetCommRealData(e.sRealType, 10));
                if (현재가 < 0)
                {
                    현재가 *= -1;
                }
                int 전일대비 = Int32.Parse(axKHOpenAPI.GetCommRealData(e.sRealType, 11));
                double 등락율 = Double.Parse(axKHOpenAPI.GetCommRealData(e.sRealType, 12));
                double 누적거래량 = Double.Parse(axKHOpenAPI.GetCommRealData(e.sRealType, 13));
                int 시간 = 체결시간 / 10000;
                int 분 = (체결시간 / 100) % 100;
                int 초 = 체결시간 % 100;
                int 실시간체결시간 = 시간 * 3600 + 분 * 60 + 초;

                //1. 단주인지 검사.(종목 혹은 범위에 따라 정해야 할듯)
                if(거래량 >= 10 && 거래량 <= -10)
                {
                    ThreadPool.QueueUserWorkItem(stockItemElement.ThreadPoolCallBack, new RealTimeTradeData(e.sRealKey, 현재가, 실시간체결시간, 거래량));
                }

                //2. 관심 종목 처리
                if(stockItemManager.stockItemDic.ContainsKey(e.sRealKey))
                {
                    stockItemManager.stockItemDic[e.sRealKey].거래량 = 거래량;
                    stockItemManager.stockItemDic[e.sRealKey].현재가 = 현재가;
                    stockItemManager.stockItemDic[e.sRealKey].등락율 = 등락율;
                    stockItemManager.stockItemDic[e.sRealKey].대비 = 전일대비;
                }
                else
                {
                    StockItem stockItem = new StockItem(axKHOpenAPI.GetMasterCodeName(e.sRealKey), 등락율, 전일대비, 현재가, 거래량);
                    stockItemManager.stockItemDic.Add(e.sRealKey, stockItem);
                }
            }
            //3. 뷰로 체결 정보 이벤트를 발생시킨다.
            OnReceiveRealDataHandler?.Invoke(this, new StockEventArgs(stockItemManager));
        }
    }
}
