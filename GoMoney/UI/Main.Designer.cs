﻿namespace UI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.axKHOpenAPI1 = new AxKHOpenAPILib.AxKHOpenAPI();
            this.panel1 = new System.Windows.Forms.Panel();
            this.단주GridView = new System.Windows.Forms.DataGridView();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SavedStockGridVeiw = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.종목코드 = new System.Windows.Forms.TextBox();
            this.종목코드등록버튼 = new System.Windows.Forms.Button();
            this.종목명label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.axKHOpenAPI1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.단주GridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SavedStockGridVeiw)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // axKHOpenAPI1
            // 
            this.axKHOpenAPI1.Enabled = true;
            this.axKHOpenAPI1.Location = new System.Drawing.Point(0, 398);
            this.axKHOpenAPI1.Name = "axKHOpenAPI1";
            this.axKHOpenAPI1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axKHOpenAPI1.OcxState")));
            this.axKHOpenAPI1.Size = new System.Drawing.Size(108, 50);
            this.axKHOpenAPI1.TabIndex = 0;
            this.axKHOpenAPI1.OnEventConnect += new AxKHOpenAPILib._DKHOpenAPIEvents_OnEventConnectEventHandler(this.axKHOpenAPI1_OnEventConnect);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.종목명label);
            this.panel1.Controls.Add(this.단주GridView);
            this.panel1.Controls.Add(this.SavedStockGridVeiw);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1383, 550);
            this.panel1.TabIndex = 1;
            // 
            // 단주GridView
            // 
            this.단주GridView.AllowUserToAddRows = false;
            this.단주GridView.AllowUserToDeleteRows = false;
            this.단주GridView.AllowUserToResizeColumns = false;
            this.단주GridView.AllowUserToResizeRows = false;
            this.단주GridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.단주GridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.단주GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.단주GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14});
            this.단주GridView.Location = new System.Drawing.Point(705, 129);
            this.단주GridView.Name = "단주GridView";
            this.단주GridView.RowHeadersVisible = false;
            this.단주GridView.RowTemplate.Height = 23;
            this.단주GridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.단주GridView.Size = new System.Drawing.Size(603, 150);
            this.단주GridView.TabIndex = 4;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "단주";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "체결횟수";
            this.Column10.Name = "Column10";
            // 
            // Column11
            // 
            this.Column11.HeaderText = "합산금액";
            this.Column11.Name = "Column11";
            // 
            // Column12
            // 
            this.Column12.HeaderText = "주기";
            this.Column12.Name = "Column12";
            // 
            // Column13
            // 
            this.Column13.HeaderText = "시작시간";
            this.Column13.Name = "Column13";
            // 
            // Column14
            // 
            this.Column14.HeaderText = "종료시간";
            this.Column14.Name = "Column14";
            // 
            // SavedStockGridVeiw
            // 
            this.SavedStockGridVeiw.AllowUserToAddRows = false;
            this.SavedStockGridVeiw.AllowUserToDeleteRows = false;
            this.SavedStockGridVeiw.AllowUserToResizeColumns = false;
            this.SavedStockGridVeiw.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.SavedStockGridVeiw.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.SavedStockGridVeiw.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SavedStockGridVeiw.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.SavedStockGridVeiw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SavedStockGridVeiw.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column8});
            this.SavedStockGridVeiw.EnableHeadersVisualStyles = false;
            this.SavedStockGridVeiw.Location = new System.Drawing.Point(12, 129);
            this.SavedStockGridVeiw.Name = "SavedStockGridVeiw";
            this.SavedStockGridVeiw.RowHeadersVisible = false;
            this.SavedStockGridVeiw.RowTemplate.Height = 23;
            this.SavedStockGridVeiw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SavedStockGridVeiw.Size = new System.Drawing.Size(643, 150);
            this.SavedStockGridVeiw.TabIndex = 3;
            this.SavedStockGridVeiw.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SavedStockGridVeiw_CellMouseClick);
            // 
            // Column7
            // 
            this.Column7.FillWeight = 20F;
            this.Column7.HeaderText = "종목코드";
            this.Column7.Name = "Column7";
            this.Column7.Visible = false;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.FillWeight = 50F;
            this.Column1.HeaderText = "번호";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "종목명";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "현재가";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.FillWeight = 50F;
            this.Column4.HeaderText = "등락율";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column5.HeaderText = "대비";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column6.HeaderText = "거래량";
            this.Column6.Name = "Column6";
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column8.FillWeight = 50F;
            this.Column8.HeaderText = "단주보기";
            this.Column8.Name = "Column8";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.종목코드);
            this.groupBox1.Controls.Add(this.종목코드등록버튼);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "관심종목등록";
            // 
            // 종목코드
            // 
            this.종목코드.Location = new System.Drawing.Point(6, 20);
            this.종목코드.Name = "종목코드";
            this.종목코드.Size = new System.Drawing.Size(151, 21);
            this.종목코드.TabIndex = 0;
            // 
            // 종목코드등록버튼
            // 
            this.종목코드등록버튼.Location = new System.Drawing.Point(6, 71);
            this.종목코드등록버튼.Name = "종목코드등록버튼";
            this.종목코드등록버튼.Size = new System.Drawing.Size(75, 23);
            this.종목코드등록버튼.TabIndex = 1;
            this.종목코드등록버튼.Text = "등록";
            this.종목코드등록버튼.UseVisualStyleBackColor = true;
            this.종목코드등록버튼.Click += new System.EventHandler(this.종목코드등록버튼_Click);
            // 
            // 종목명label
            // 
            this.종목명label.AutoSize = true;
            this.종목명label.Location = new System.Drawing.Point(705, 99);
            this.종목명label.Name = "종목명label";
            this.종목명label.Size = new System.Drawing.Size(41, 12);
            this.종목명label.TabIndex = 5;
            this.종목명label.Text = "종목명";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1383, 550);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.axKHOpenAPI1);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axKHOpenAPI1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.단주GridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SavedStockGridVeiw)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button 종목코드등록버튼;
        private System.Windows.Forms.TextBox 종목코드;
        private System.Windows.Forms.DataGridView SavedStockGridVeiw;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridView 단주GridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.Label 종목명label;
    }
}